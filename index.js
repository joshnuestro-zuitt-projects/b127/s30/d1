const express = require('express')
//Mongoose is a package that allows creation of schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose')
const app = express()
const port = 3001

//connect to the database by passing in your connection string, remember to replace <password> and database name with actual valuse
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.vlaci.mongodb.net/batch127_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
//Connecting to robo3t locally
/*
mongoose.connect("mongodb://localhost:27017/database name"), {
	useNewUrlParser: true,
	useUnifiedTopology: true	
})
*/

//set notifications for connection success or failure
//connection to the database allows us to handle errors when the initial connection is established
let db = mongoose.connection
//If a connection error occurs, output in the console
//console.error.bind(console) = allows us to print error in the browser console and in the terminal

db.on("error", console.error.bind(console, "connection error"))
//if the connection is successful, output in the console
db.once("open", ()=>console.log("We're connected to the cloud database"))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Mongoose schemas
//schemas determine the structure of the documents to be written in the database
//schemas act as blueprints to our data
//use the Schema() constructor of the Mongoose module to create a new Schema object
//The "new" keyword create a new Schema
const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding datatype
	//for task, it neds a "name" and "status"
	name: String,
	status: {
		type: String,
		//default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
})
//Task is capitalized following the MVC approach for naming convention
//Model-View-Controller
const Task = mongoose.model("Task", taskSchema)
//The first parameter of the Mongoose model method indicates the collection in where to store the data
//The second parameter is used to specify the Schema/blueprint of the documents that wil be stored in the MongoDB collection


//Create a new task
/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
 - if the task already exists in the database, we return an error
 - if the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema default is "pending" upon creation of an object

*/

app.post('/tasks', (req,res)=>{
	//check if there are duplicate tasks
	//findOne is a mongoose method that acts similar to "find" of mongodb
	//findOne() returns the first document that matches the search criteria
	Task.findOne({name: req.body.name}, (err, result)=>{
		//if a document is found and the document's name matches the information sent via client/postman
		if(result!=null && result.name == req.body.name){
			//return a message to the client
			return res.send("Duplicate task found")
		}
		else{
			//if no document is found, create a new task and save it to our database
			let newTask=new Task({
				name: req.body.name
			})
			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send(`New task \"${req.body.name}\" created`)
				}
			})
		}
	})
})

//Getting all the tasks

/*
Business Logic
1. We will retrieve all the documents using "GET"
2. If an error is encountered, print error
3. If there's no error, send a success status back to the client/postmand and return an array of document

*/

app.get('/tasks', (req,res)=>{
	//"find" is a mongoose method that is similar to MongoDB "find"
	//empty object '{}' means return ALL the documents and stores them in the result parameter of the callback function
	Task.find({}, (err,result)=>{
		//if an error occurs
		if(err){
			return console.log(err)
		}
		else{
			//if no errors are found
			//status 200 means that everything is OK in terms of processing
			//the 'json' method allows to send a JSON format for the response
			//the returned response is purposefully returned as an object with the 'data' property to mirror real world complex data structures
			return res.status(200).json({
				data: result
			})
		}
	})
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema)

app.post('/signup', (req,res)=>{
	User.findOne({username: req.body.username}, (err, result)=>{
		if(result!=null && result.username == req.body.username){
			return res.send(`Username \"${result.username}\" is taken`)
		}
		else{
			let newUser=new User({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save((saveErr, savedUser)=>{
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send(`New user \"${req.body.username}\" added`)
				}
			})
		}
	})
})

app.get('/users', (req,res)=>{
	User.find({}, (err,result)=>{
		if(err){
			return console.log(err)
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))